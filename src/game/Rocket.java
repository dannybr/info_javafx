package game;

public class Rocket extends Element{

	public void update(double time, double screenWidth, double screenHeight) {
		if(getVelocityX() > 0) {
			if(getPositionX() + getVelocityX() * time + getWidth() < screenWidth -15) {
				setPositionX(getPositionX() + getVelocityX() * time);
			}
		}
		if(getVelocityX() < 0) {
			if(getPositionX() + getVelocityX() * time > 0) {
				setPositionX(getPositionX() + getVelocityX() * time);
			}
		}
		if(getVelocityY() > 0) {
			if(getPositionY() + getVelocityY() * time + getHeight() < screenHeight - getHeight() + 10) {
				setPositionY(getPositionY() + getVelocityY() * time);
			}
		}
		if(getVelocityY() < 0) {
			if(getPositionY() + getVelocityY() * time > 0) {
				setPositionY(getPositionY() + getVelocityY() * time);
			}
		}
	}
	
}
