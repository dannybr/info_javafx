package game;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JOptionPane;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class Game extends Application {

	private final double ROCKET_SPEED = 250;
	private final double SHOT_SPEED = 500;
	private final double MAX_ALIEN_SPEED = 400;
	private final double TIME_BETWEEN_SHOTS = 0.3;
	private final double TIME_BETWEEN_ALIENS = 0.3;

	private final int NEGATIVE_POINTS_IF_ALIEN_PASSES = 2;
	private final int MAX_NEGATIVE_POINTS = 50;

	private List<String> input = new ArrayList<String>();
	private double screenWidth;
	private double screenHeight;
	private Scene scene;

	private List<Element> shots = new ArrayList<Element>();
	private DoubleValue timeSinceLastShot = new DoubleValue(0);
	private List<Element> aliens = new ArrayList<Element>();
	private DoubleValue timeSinceLastAlien = new DoubleValue(0);
	
	DatabaseHandler dbhand = new DatabaseHandler();
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws SQLException, ClassNotFoundException {
		GraphicsContext gc = initStageAndGetGraphicsContext(stage);
		
		Image space = new Image("space.png");

		Rocket rocket = new Rocket();
		rocket.setImage("rakete120.png");
		rocket.setPosition(200, 380);

		LongValue lastNanoTime = new LongValue(System.nanoTime());

		IntValue score = new IntValue(0);
		IntValue highscore = new IntValue(0);

		new AnimationTimer() {

			@Override
			public void stop() {
				String overText = "GAME OVER";
				Font overFont = Font.font("Symbol", FontWeight.BOLD, 60);
				gc.setFont(overFont);
				gc.fillText(overText, screenWidth / 2 - 200, screenHeight / 5);
				
				String highscoreTitle = "Highscores:";
				Font highscoreTitleFont = Font.font("Symbol", FontWeight.BOLD, 40);
				gc.setFont(highscoreTitleFont);
				gc.fillText(highscoreTitle, screenWidth / 2 - 200, screenHeight / 5 + 70);
				
				String userName = JOptionPane.showInputDialog("Enter your name: ");
				
				dbhand.addHighscore(userName, highscore.value);
				ResultSet highscoreResultSet = dbhand.getHighscore();
				try {
					int count = 1;
					while(highscoreResultSet.next()) {
						String highscoreText = highscoreResultSet.getString("Name") + " : " + highscoreResultSet.getString("Highscore");
						Font highscoreTextFont = Font.font("Symbol", FontWeight.BOLD, 20);
						gc.setFont(highscoreTextFont);
						gc.fillText(highscoreText, screenWidth / 2 - 100, screenHeight / 5 + 100 + count * 30);
						count++;
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
				super.stop();
			}

			@Override
			public void handle(long currentNanoTime) {

				if (score.value <= -MAX_NEGATIVE_POINTS) {
					stop();
					return;
				}

				double elapsedTime = (currentNanoTime - lastNanoTime.value) / 1000000000.0;
				lastNanoTime.value = currentNanoTime;
				timeSinceLastShot.value += elapsedTime;
				timeSinceLastAlien.value += elapsedTime;

				handleRocketMovement(rocket);

				rocket.update(elapsedTime, screenWidth, screenHeight);
				gc.drawImage(space, 0, 0, screenWidth, screenHeight);
				rocket.render(gc);

				String pointsText = ("Score: " + (score.value));
				gc.fillText(pointsText, 360, 36);
				gc.strokeText(pointsText, 360, 36);

				String highscoreText = ("Highscore: " + (highscore.value));
				gc.fillText(highscoreText, 750, 36);
				gc.strokeText(highscoreText, 750, 36);

				if (timeSinceLastAlien.value >= TIME_BETWEEN_ALIENS) {
					addNewAlien();
					timeSinceLastAlien.value = 0;
				}

				if (input.contains("SPACE") && timeSinceLastShot.value >= TIME_BETWEEN_SHOTS) {
					addNewShot(rocket);
					timeSinceLastShot.value = 0;
				}
				Iterator<Element> shotIterator = shots.listIterator();
				while (shotIterator.hasNext()) {
					Element shot = shotIterator.next();
					if (shot.getPositionX() > screenWidth) {
						shotIterator.remove();
					} else {
						shot.update(elapsedTime);
						shot.render(gc);
					}

					Iterator<Element> alienIter = aliens.iterator();
					while (alienIter.hasNext()) {
						Element alien = alienIter.next();
						if (shot.intersects(alien)) {
							alienIter.remove();
							score.value++;
							shotIterator.remove();
							if (score.value > highscore.value)
								highscore.value = score.value;
						}
					}

				}

				Iterator<Element> alienIter = aliens.iterator();
				while (alienIter.hasNext()) {
					Element alien = alienIter.next();
					alien.update(elapsedTime);
					alien.render(gc);
					if (rocket.intersects(alien)) {
						stop();
					}

					if (alien.getPositionX() < 10) {
						alienIter.remove();
						score.value -= NEGATIVE_POINTS_IF_ALIEN_PASSES;
					}
				}

			}

			private void addNewShot(Rocket rocket) {
				Element shot = new Element();
				shot.setImage("shot2.png");
				shot.setPositionY(rocket.getPositionY() + rocket.getHeight() / 2 - shot.getHeight() / 2);
				shot.setPositionX(rocket.getPositionX() + 100);
				shot.setVelocityY(0);
				shot.setVelocityX(SHOT_SPEED);
				shots.add(shot);
			}

			private void addNewAlien() {
				Element newAlien = new Element();
				newAlien.setImage("alien.png");
				double px = screenWidth - newAlien.getWidth();
				double py = (screenHeight - 30 - newAlien.getHeight()) * Math.random();
				newAlien.setPosition(px, py);
				newAlien.setVelocityX(Math.random() * -MAX_ALIEN_SPEED);
				aliens.add(newAlien);
			}

		}.start();

		stage.show();
	}

	private GraphicsContext initStageAndGetGraphicsContext(Stage stage) {
		setScreenSizes();

		stage.setTitle("Rocket!");

		Group root = new Group();
		scene = new Scene(root);
		stage.setScene(scene);

		stage.setWidth(screenWidth);
		stage.setHeight(screenHeight);

		Canvas canvas = new Canvas(screenWidth, screenHeight);
		root.getChildren().add(canvas);

		handleKeyEvents();

		GraphicsContext gc = canvas.getGraphicsContext2D();

		Font theFont = Font.font("Symbol", FontWeight.BOLD, 24);
		gc.setFont(theFont);
		gc.setFill(Color.RED);
		gc.setStroke(Color.BLACK);
		gc.setLineWidth(1);

		return gc;
	}

	private void handleKeyEvents() {
		scene.setOnKeyPressed(keyEvent -> {
			String code = keyEvent.getCode().toString();
			if(code.equals("ESCAPE"))
				System.exit(0);
			if (!input.contains(code))
				input.add(code);
		});

		scene.setOnKeyReleased(keyEvent -> {
			String code = keyEvent.getCode().toString();
			input.remove(code);
		});
	}

	private void setScreenSizes() {
		Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
		screenWidth = primaryScreenBounds.getWidth();
		screenHeight = primaryScreenBounds.getHeight();
	}

	private void handleRocketMovement(Element rocket) {
		rocket.setVelocity(0, 0);
		if (input.contains("LEFT"))
			rocket.addVelocity(-ROCKET_SPEED, 0);
		if (input.contains("RIGHT"))
			rocket.addVelocity(ROCKET_SPEED, 0);
		if (input.contains("UP"))
			rocket.addVelocity(0, -ROCKET_SPEED);
		if (input.contains("DOWN"))
			rocket.addVelocity(0, ROCKET_SPEED);
	}

}
