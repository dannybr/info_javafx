package game;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;



public class DatabaseHandler {
	
	private Connection conn;
	
	private Connection getConnection() throws SQLException, ClassNotFoundException {

		Properties connectionProps = new Properties();
		connectionProps.put("user", "root");
		connectionProps.put("password", "");
		
		Class.forName("com.mysql.cj.jdbc.Driver");

		return DriverManager.getConnection("jdbc:mysql://localhost:3306/", connectionProps);
	}

	public ResultSet getHighscore() {
		try {
			Statement stmt = conn.createStatement();
			return stmt.executeQuery("select Name, Highscore from kekgame.scores order by highscore desc limit 10");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void addHighscore(String name, int score) {
		try {
			Statement stmt = conn.createStatement();
			stmt.executeUpdate("INSERT INTO kekgame.scores (name, highscore) VALUES ('" + name + "', " + score + ")");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public DatabaseHandler() {
		try {
			this.conn = getConnection();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

}